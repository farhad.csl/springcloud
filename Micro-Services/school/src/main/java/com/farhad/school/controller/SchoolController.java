package com.farhad.school.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.farhad.school.dto.FullSchoolResponse;
import com.farhad.school.model.School;
import com.farhad.school.service.SchoolService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/schools")
@RequiredArgsConstructor
public class SchoolController {

	
	private final SchoolService schoolService;
	
	@PostMapping
	public ResponseEntity<School> createSchool(@RequestBody School school){
		return new ResponseEntity<>(schoolService.saveSchool(school),HttpStatus.CREATED);
	} 
	@GetMapping 	
  	public ResponseEntity<List<School>> findAllSchools(){
  		return new ResponseEntity<>(schoolService.findAllSchools(),HttpStatus.FOUND);
  	}
	
	@GetMapping("/with-student/{school-id}") 	
  	public ResponseEntity<FullSchoolResponse> findAllSchools(@PathVariable("school-id") Integer schoolId){
  		return new ResponseEntity<>(schoolService.findSchoolsWithStudents(schoolId),HttpStatus.FOUND);
  	}
}
