package com.farhad.student.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.farhad.student.model.Student;
import com.farhad.student.model.repository.StudentRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class StudentService {
	
	
	private final StudentRepository studentRepository;
	
	public Student saveStudent(Student student) {
		return studentRepository.save(student);
	}
	
	public List<Student> findAllStudents(){
		return studentRepository.findAll();
	}
	
	public List<Student> findAllStudentsBySchool(Integer schoolId){
		return studentRepository.findAllBySchoolId(schoolId);
	} 

}
