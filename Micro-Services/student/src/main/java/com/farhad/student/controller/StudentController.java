package com.farhad.student.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.farhad.student.model.Student;
import com.farhad.student.service.StudentService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("api/v1/students")
@RequiredArgsConstructor
public class StudentController {
	
	private final StudentService studentService;
	
	@PostMapping
	public ResponseEntity<Student> save(@RequestBody Student student) {
		return new ResponseEntity<>(studentService.saveStudent(student),HttpStatus.CREATED) ;
	}
	
	
	@GetMapping 	
  	public ResponseEntity<List<Student>> findAllStudents(){
  		return new ResponseEntity<>(studentService.findAllStudents(),HttpStatus.FOUND);
  	}
	
	@GetMapping("/school/{school-id}") 	
  	public ResponseEntity<List<Student>> findAllStudents(@PathVariable("school-id") Integer schoolId){
  		return new ResponseEntity<>(studentService.findAllStudentsBySchool(schoolId),HttpStatus.FOUND);
  	}

}
